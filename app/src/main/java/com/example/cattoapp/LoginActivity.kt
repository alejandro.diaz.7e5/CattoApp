package com.example.cattoapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.cattoapp.ui.theme.CattoAppTheme
import com.example.cattoapp.ui.viewmodel.LoginViewModel

class LoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?)
    {

        super.onCreate(savedInstanceState)
        setContent {
            val model : LoginViewModel = viewModel()
            model.loadLoginModel()
            CattoAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    loginLayout(model, this@LoginActivity)
                }
            }
        }
    }
}

@Composable
fun loginLayout(model: LoginViewModel, context: Context)
{
    var username by remember { mutableStateOf(TextFieldValue("")) }
    var password by remember { mutableStateOf(TextFieldValue("")) }
    Column(
        modifier = Modifier.fillMaxWidth(),
    ) {
        Row(modifier = Modifier.fillMaxWidth(),horizontalArrangement = Arrangement.Center) {
            Text(text = "Please login to proceed")
        }
        Row(modifier = Modifier.fillMaxWidth(),horizontalArrangement = Arrangement.Center) {
            TextField(
                value = username,
                onValueChange = { username = it },
                label = { Text("Username") },
                maxLines = 1,
                modifier = Modifier.padding(20.dp)
            )
        }
        Row(modifier = Modifier.fillMaxWidth(),horizontalArrangement = Arrangement.Center) {
            TextField(
                value = password,
                onValueChange = { password = it },
                label = { Text("Password") },
                maxLines = 1,
                modifier = Modifier.padding(20.dp)
            )

        }
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
            val intent = Intent(context, MainActivity::class.java)
            //val wrongUsernameOrPassword = Toast.makeText(context, "Wrong username or password", Toast.LENGTH_LONG)
            val openDialog = remember { mutableStateOf(false)  }
            if (openDialog.value) {
                AlertDialog(
                    onDismissRequest = { openDialog.value = false },
                    title = { Text(text = "Error") },
                    text = { Text("Wrong username or password") },
                    confirmButton = {
                        Button(
                            onClick = {
                                openDialog.value = false
                            }) {
                            Text("OK")
                        }
                    }
                )
            }
            Button(onClick = {if(model.checkLogin(username.text, password.text))
                context.startActivity(intent)
            else
                openDialog.value = true
            }) {
                Text(text = "Login")
            }
        }
    }
}
