package com.example.cattoapp.ui.model.mapper

import com.example.cattoapp.ui.model.CatDetailUiModel

import com.example.cattoapp.data.apiservice.model.CatBreedDto
import com.example.cattoapp.data.apiservice.model.CatImageDto
import com.example.cattoapp.ui.model.CatDataUiModel

class CatDetailUiMapper {
    fun map(image :CatImageDto, breed :CatBreedDto):CatDetailUiModel{
        return CatDetailUiModel(
            catImage = image.img_url,
            catName = breed.name,
            description = breed.description,
            countryCode = breed.countryCode,
            temperament = breed.temperament,
            wikiLink = breed.wikipediaUrl
        )
    }
}