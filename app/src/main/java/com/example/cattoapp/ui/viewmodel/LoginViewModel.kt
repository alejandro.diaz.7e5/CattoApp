package com.example.cattoapp.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.cattoapp.data.login.LoginDto
import com.example.cattoapp.data.login.SampleList
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlin.reflect.jvm.internal.impl.util.Check

class LoginViewModel : ViewModel() {
    private val _uiState: MutableStateFlow<List<LoginDto>?> = MutableStateFlow(null)
    val uiState: StateFlow<List<LoginDto>?> = _uiState.asStateFlow()

    fun loadLoginModel()
    {
        _uiState.value = SampleList
    }
    fun checkLogin( username:String, password: String ):Boolean
    {
        return (_uiState.value?.find{ it.username == username && it.password == password} != null)
    }
}