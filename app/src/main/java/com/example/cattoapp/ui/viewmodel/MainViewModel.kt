package com.example.cattoapp.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cattoapp.data.apiservice.CatApi
import com.example.cattoapp.data.apiservice.model.CatImageDto
import com.example.cattoapp.ui.model.mapper.CatDataUIMapper
import com.example.cattoapp.ui.model.CatDataUiModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory




private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

var retrofit = Retrofit.Builder()
    .baseUrl("https://api.thecatapi.com/v1/")
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .build()
private val mapper = CatDataUIMapper()

class MainViewModel : ViewModel() {
    private val _uiState: MutableStateFlow<List<CatDataUiModel>?> = MutableStateFlow(null)
    val uiState: StateFlow<List<CatDataUiModel>?> = _uiState.asStateFlow()
    /*fun loadOneCatImage() {
        viewModelScope.launch {
             CatApi.retrofitService.getPhotos(limit = null, hasBreeds = null)
        }
    }
    fun loadTenCatData(){
        viewModelScope.launch {
            CatApi.retrofitService.getPhotos(limit = 10, hasBreeds = null)
        }
    }*/
    fun loadUiModel(){
        viewModelScope.launch {
                val breeds = CatApi.retrofitService.getBreeds()
                val images = breeds.flatMap {
                    //CatApi.retrofitService.getPhotos(limit = null, breed_ids = it.id) ?:listOf(CatImageDto(it.id, "https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg", 500, 500, null))
                    CatApi.retrofitService.getPhotos(limit = null, breed_ids = it.id).ifEmpty {
                            listOf(
                                CatImageDto(
                                    it.id,
                                    "https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg",
                                    500,
                                    500,
                                    null
                                )
                            )
                        }
                    }

                /*val images : MutableList<CatImageDto> = mutableListOf()
                for (breed :CatBreedDto in breeds)
                {
                    val imageL:List<CatImageDto>? = CatApi.retrofitService.getPhotos(limit = null, breed_ids = breed.id)
                    if (imageL != null) {
                        (if (imageL.isEmpty())
                            CatImageDto("noimg", "https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg", 500, 500, null)
                        else
                            imageL[0])
                    }
                }*/
                _uiState.value = mapper.map(images, breeds)
            }
        }
    fun GetBreedCountryCodes() :List<String>{
            return uiState.value!!.map{it.countryCode}.distinct()
        }
    }
