package com.example.cattoapp.ui.model

data class CatDetailUiModel(
    val catImage :String,
    val catName :String,
    val description :String,
    val countryCode :String,
    val temperament :String,
    val wikiLink :String?,
)