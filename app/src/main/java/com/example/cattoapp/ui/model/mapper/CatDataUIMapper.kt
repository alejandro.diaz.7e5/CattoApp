package com.example.cattoapp.ui.model.mapper

import com.example.cattoapp.data.apiservice.model.CatBreedDto
import com.example.cattoapp.data.apiservice.model.CatImageDto
import com.example.cattoapp.ui.model.CatDataUiModel

class CatDataUIMapper {
    fun map(images :List<CatImageDto>, breeds :List<CatBreedDto>):List<CatDataUiModel>{
       return breeds.zip(images).map { (breeds, images) -> CatDataUiModel( id = breeds.id, name = breeds.name, imageUrl = images.img_url, countryCode = breeds.countryCode) }
    }
}