package com.example.cattoapp.ui.model

data class CatDataUiModel(
    val id :String,
    val name :String,
    val imageUrl :String,
    val countryCode :String,
)
