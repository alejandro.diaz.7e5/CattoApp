package com.example.cattoapp.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cattoapp.data.apiservice.CatApi
import com.example.cattoapp.ui.model.CatDetailUiModel
import com.example.cattoapp.ui.model.mapper.CatDetailUiMapper
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private val mapper = CatDetailUiMapper()

class DetailViewModel : ViewModel() {
    private val _uiState: MutableStateFlow<CatDetailUiModel?> = MutableStateFlow(null)
    val uiState: StateFlow<CatDetailUiModel?> = _uiState.asStateFlow()

    fun loadUiModel(breedID :String){
        viewModelScope.launch {
            val breed = CatApi.retrofitService.getBreeds().find{ it.id == breedID }
            val image = CatApi.retrofitService.getPhotos(limit = null, breed_ids = breedID)?.get(0)

            _uiState.value = mapper.map(image!!, breed!!)
        }
    }
}

