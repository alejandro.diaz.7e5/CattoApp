package com.example.cattoapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.cattoapp.ui.model.CatDetailUiModel
import com.example.cattoapp.ui.theme.CattoAppTheme
import com.example.cattoapp.ui.viewmodel.DetailViewModel

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val model : DetailViewModel = viewModel()
            model.loadUiModel(intent.extras?.getString("breedID")!!)
            val cat by model.uiState.collectAsState()
            CattoAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    cat?.let {
                        (CattoDetailDisplay(detail = it))
                    }
                }
            }
        }
    }
}




@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun CattoDetailDisplay(detail :CatDetailUiModel) {
    // TODO 3. Wrap affirmation card in a lazy column
    Column(
        modifier = Modifier.fillMaxWidth().verticalScroll(rememberScrollState()),
    ) {
        Row() {
            GlideImage(model = detail.catImage, contentDescription = "A cat Image",   modifier =
            Modifier
                .fillMaxWidth()
                .padding(20.dp)
                .wrapContentHeight()
                .border(width = 10.dp, color = Color.Cyan, shape = RoundedCornerShape(24.dp))
                .clip(shape = RoundedCornerShape(24.dp)),)
        }
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(30.dp),
            horizontalArrangement = Arrangement.Center) {
            Text(text = detail.catName, color = Color.Magenta, fontSize = 30.sp)

        }
        Row( modifier = Modifier.padding(20.dp)
            .border(5.dp, color = Color.Blue, shape = RoundedCornerShape(10.dp))
            .padding(15.dp)) {
            Text(text = detail.description,  fontSize = 20.sp)
        }
        Row() {
            Text(text = detail.countryCode)
        }
        Row() {
            Text(text = detail.temperament)
        }
        Row() {
            val uriHandler = LocalUriHandler.current
            Text(
                text = "Wikipedia Link",
                color = Color.DarkGray,
                fontWeight = FontWeight.W900,
                modifier = Modifier.clickable(
                    enabled = true,
                    onClick = { uriHandler.openUri(detail.wikiLink!!) })
            )
        }
    }
}