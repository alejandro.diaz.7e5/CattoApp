package com.example.cattoapp.data.apiservice.model

import com.squareup.moshi.Json

data class CatImageDto(
     @Json(name = "id") val id :String,
     @Json(name = "url") val img_url:String,
     @Json(name = "width") val img_width:Int,
     @Json(name = "height") val img_height: Int,
     @Json(name = "breeds") val breed: CatBreedDto?
)

