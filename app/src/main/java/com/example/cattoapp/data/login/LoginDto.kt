package com.example.cattoapp.data.login



data class LoginDto
    (
    val username :String,
    val password :String
    )

val SampleList :List<LoginDto> = listOf(
    LoginDto("pepe", "1234"),
    LoginDto("carlos", "3368"),
    LoginDto("jose", "3245"),
    LoginDto("mario", "7568"),
    LoginDto("alex", "2879"),
    LoginDto("pedro", "2436"),
    LoginDto("felipe", "2346"),
    LoginDto("luis", "2354"),
    LoginDto("david", "2239"),
)