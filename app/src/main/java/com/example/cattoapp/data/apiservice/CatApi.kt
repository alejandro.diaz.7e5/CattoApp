package com.example.cattoapp.data.apiservice

import com.example.cattoapp.data.apiservice.model.CatBreedDto
import com.example.cattoapp.data.apiservice.model.CatImageDto
import com.example.cattoapp.ui.viewmodel.retrofit
import retrofit2.http.GET
import retrofit2.http.Query

interface CatApiService
    {
        @GET("images/search")
        suspend fun getPhotos(
            @Query("limit") limit: Int?,
            @Query("breed_ids") breed_ids : String?
        ): List<CatImageDto>
        @GET("breeds")
        suspend fun getBreeds(
        ): List<CatBreedDto>
    }

object CatApi {
    val retrofitService : CatApiService by lazy {
        retrofit.create(CatApiService::class.java) }
}