package com.example.cattoapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.cattoapp.ui.model.CatDataUiModel
import com.example.cattoapp.ui.theme.CattoAppTheme
import com.example.cattoapp.ui.viewmodel.MainViewModel
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val model :MainViewModel = viewModel()
            model.loadUiModel()
            val cat by model.uiState.collectAsState()
            val context = this@MainActivity
            CattoAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    cat?.let {
                        (CattoListDisplay(catList = it, context = context, model = model))
                }
            }
        }
    }
}
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun CattoCard(Data : CatDataUiModel, context: Context){
    Card(

        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp)
            .wrapContentHeight()
            .border(width = 10.dp, color = Color.Cyan, shape = RoundedCornerShape(24.dp))
            .clip(shape = RoundedCornerShape(24.dp)),

    ) {
    Column() {
    val intent = Intent(context, DetailActivity::class.java).putExtra("breedID", Data.id)

    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(30.dp),
        horizontalArrangement = Arrangement.Center) {
        Text(text = Data.name, color = Color.Magenta, fontSize = 30.sp)

    }
    Row() {

        GlideImage(contentScale = ContentScale.FillWidth,model = Data.imageUrl, contentDescription = "Cat pic",alignment = Alignment.Center , modifier =
        Modifier
            .clickable { context.startActivity(intent) }
            .fillMaxWidth())
    }
}
}
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    CattoAppTheme {
        Greeting("Android")
    }
}

@Composable
fun CattoListDisplay(catList: List<CatDataUiModel>, modifier: Modifier = Modifier, context: Context, model: MainViewModel) {
    // TODO 3. Wrap affirmation card in a lazy column
    Row(modifier = Modifier.padding(40.dp)){
        Country_DropDownMenu(model = model)

    }
    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ) {
        items(catList.count()) { index ->
            CattoCard(Data = catList[index], context = context)
        }
    }
}
@Composable
fun Country_DropDownMenu(model: MainViewModel) {
    val context = LocalContext.current
    var expanded by remember { mutableStateOf(false) }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentSize(Alignment.TopEnd)
    ) {
        IconButton(onClick = { expanded = !expanded }) {
            Icon(
                imageVector = Icons.Default.MoreVert,
                contentDescription = "More"
            )
        }

        DropdownMenu(
            expanded = expanded,

            onDismissRequest = { expanded = false }
        ) {
            val items = model.GetBreedCountryCodes()
            var selectedCountry by remember { mutableStateOf("") }
            items.forEach {s ->
                DropdownMenuItem(onClick = {
                    selectedCountry = s
                    expanded = false
                }) {

                }
            }
        }
    }
}